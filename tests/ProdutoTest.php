<?php

namespace App\Models;


class ProdutoTest extends \PHPUnit\Framework\TestCase
{
	public function testProdutoType()
	{
		$produto = new Produto();
		$this->assertInstanceOf(ProdutoInterface::class, $produto);
	}

	public function testProdutoName()
	{
		$produto = new Produto();
		$produto->setname('Sabao');
		$this->assertEquals($produto->getName(), "Sabao");
	}

	public function testProdutoDescricao()
	{
		$produto = new Produto();
		$produto->setdescription('desc');
		$this->assertEquals($produto->getDescription(), "desc");
	}

	public function testProdutoPreco()
	{
		$produto = new Produto();
		$produto->setprice(10);
		$this->assertEquals($produto->getPrice(), 10);
	}

	/**
	 *@expectedException InvalidArgumentException
	 */

	public function testProdutoPriceNotAndNumeric()
	{
		$produto = new Produto();
		$produto->setPrice('text');
	}

	public function testCodeBar()
	{
		$produto = new Produto();
		$produto->setcodebar(123456);
		$this->assertEquals($produto->getCodebar(), 123456);
	}

	public function testProdutoMarca()
	{
		$produto = new Produto();
		$produto->setmarca('SENAI');
		$this->assertEquals($produto->getMarca(), "SENAI");
	}
}

?>