<?php

namespace App\Models;

use ArrayObject;

class Order{
	private $produto;

	function __construct() {
		$this->produto = new ArrayObject();
	}

	public function addProduto($value='')
	{
		$this->produto->append($value);
		return $this;
	}
	
}