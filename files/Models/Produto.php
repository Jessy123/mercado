<?php

namespace App\Models;

class Produto implements ProdutoInterface
{
	private $name;
	private $description;
	private $price;
	private $codebar;
	private $marcar;

	public function set($value='')
	{
		return true;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setPrice($price)
	{
		if (!is_numeric($price)) {

			throw new \InvalidArgumentException;

		}

		$this->price = $price;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setCodebar($codebar)
	{
	
		if (strlen($codebar) != 6) {
			throw new \InvalidArgumentException('Erro de argumento do código de barras', 2);
			
		}
		$this->codebar = $codebar;
	}

	public function getCodebar()
	{
		return $this->codebar;
	}

	public function setMarca($marca)
	{
		$this->marca = $marca;
	}

	public function getMarca()
	{
		return $this->marca;
	}
}

?>