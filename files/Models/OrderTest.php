<?php

namespace App\Models;

Class OrderTest extends PHPUnit\Framework\TestCase
{
	public function testGetProdutoList()
	{
		$produto1 = new Produto();
		$produto1->setName('Produto 1');
		$produto1->setDescription('Desc 1');
		$produto1->setCodeBar(123456789012);
		$produto1->setPrice(10);

		$produto2 = new Produto();
		$produto2->setName('Produto 2');
		$produto2->setDescription('Desc 2');
		$produto2->setCodeBar(123456789312);
		$produto2->setPrice(70);

		//Order para adicionar os produtos dentro do carinho de compra
        
        $order = new Order();
        $order->addProduto($produto1);
        $order->addProduto($produto2);

        $produtos = new \ArrayObject([$produto1, $produto2]);

        $this->assertEquals($produtos, $order->getProdutos());

	}
}

?>
	
